<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pievienot auto</title>
    <style>
        div {
            text-decoration: none;
            text-align: center;
            padding-top: 1%;
            display: block;
            font-size: 20pt;
        }
    </style>
</head>
<body>
<FORM><INPUT Type="button" VALUE="Atpakal" onClick="history.go(-1);return true;"></FORM>
<div>
<form method="POST" action="/" enctype="multipart/form-data">
    {{csrf_field()}}
    <label for="names_id">Markas id cipars</label>
    <input type="number" name="names_id">
    <br>
    <label for="modelis">Modelis</label>
    <input type="text" name="modelis">
    <br>
    <label for="gads">Gads</label>
    <input type="number" name="gads">
    <br>
    <input type="file" name="upload">
    <button type="submit">Save Car</button>
</form>
</div>
<h4>Markas un to id cipars</h4>
@foreach($names as $name)
    {{$name->marka}} -
    {{$name->id}}
@endforeach

</body>
</html>