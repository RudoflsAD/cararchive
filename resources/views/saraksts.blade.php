<!<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Saraksts</title>
    <style>
        a {
            text-decoration: none;
            text-align: center;
            padding-top: 1%;
            display: block;
            font-size: 20pt;
            color: #0f0f0f;
        }
        ul {
            position: relative;
            list-style-type: none;
        }
        div{
            width: 20%;
            position: relative;
            list-style-type: none;
            padding-top: 2%;
            left: 35%;
        }
    </style>
</head>
<body>
<FORM><INPUT Type="button" VALUE="Atpakal" onClick="history.go(-1);return true;"></FORM>
<div>
<ul>
    @foreach ($names as $name)
        <li>

            <a href="/cars/{{$name->id}}">

                {{ $name->marka }}
            </a>

        </li>

    @endforeach
</ul>
</div>
</body>
</html>