<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.css">
    <title>{{$car->marka}}</title>    <style>
        div {
            text-align: center;
            padding-top: 1%;
            display: block;
            font-size: 20pt;
        }
        a {
            text-decoration: none;
            color: #7da8c3;
        }
        td{
            width: 33.33%;
        }

    </style>
</head>

<body>
<FORM><INPUT Type="button" VALUE="Atpakal" onClick="history.go(-1);return true;"></FORM>
<div>
<h2>{{$car->marka}}</h2>
    @foreach( $car->car as $car )
        <table style="width:100%">
            <tr>
                <th>Modelis</th>
                <th>Gads</th>
                <th>Bilde</th>
            </tr>
            <tr>
                <td>{{$car->modelis}}</td>
                <td>{{$car->gads}}</td>
                <td><img src="{{asset($car->image)}}" height="200px"></td>
                <td class="center"><a href="/cars/delete/{{$car->id}}">Dzest</a></td>
            </tr>

        </table>
@endforeach
</body>
</html>
