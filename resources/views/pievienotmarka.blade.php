<!<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pievienot markas</title>
    <style>
        div {
            text-decoration: none;
            text-align: center;
            padding-top: 1%;
            display: block;
            font-size: 20pt;
        }
    </style>
</head>
<body>
<FORM><INPUT Type="button" VALUE="Atpakal" onClick="history.go(-1);return true;"></FORM>
<div>
<form method="POST" action="/marka">
    {{csrf_field()}}
    <label for="marka">Marka</label>
    <input type="text" name="marka">
    <button type="submit">Pievienot</button>
</form>
</div>
</body>
</html>