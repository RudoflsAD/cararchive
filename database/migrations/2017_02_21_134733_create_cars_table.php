<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('names_id')->unsigned();
            $table->string('image');
            $table->string('modelis')->default('0');
            $table->integer('gads')->default('0');
            $table->timestamps();
        });
        Schema::table('cars', function (Blueprint $table) {
            $table->foreign('names_id')->references('id')->on('names');
    });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
