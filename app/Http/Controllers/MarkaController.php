<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\names;
use App\Car;
class MarkaController extends Controller
{
    public function index()
    {
        $names = names::all();
        return view('pievienotmarka', compact('names'));
    }
    public function store()
    {
        names::create([
            'marka' => request('marka')
        ]);
        return back();
    }

}
