<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Car;
use App\names;

class CarController extends Controller
{

    public function index()
    {
        return view('welcome');
    }
    public function showlist()
    {
        $names = names::orderBy("marka")->get();

        return view('saraksts', compact('names'));
    }

    public function show(names $car)
    {
        return view('markas', compact('car'));
    }
    public function showimage()
    {
        $cars = Car::all();
        return view('bilde', compact('cars'));
    }
    public function delete($id)
    {

        Car::find($id)->delete();

        return back();

    }

}
