<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use App\names;

class CreateController extends Controller
{
    public function index()
    {
        $names = names::all();
        return view('pievienot', compact('names'));
    }
    public function save()
    {
        $file = request()->file('upload');
        $file->store('uploads');
        $path = $file->store('uploads');

        \DB::table('cars')->insert([
            'modelis' => request('modelis'),
            'gads' => request('gads'),
            'names_id' => request('names_id'),
            'image' => $path
        ]);
        // request()->file('upload')->store('uploads');
        return back();
    }


}
