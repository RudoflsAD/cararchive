<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CarController@index');
Route::get('/list', 'CarController@showlist');
Route::get('/cars/{car}', 'CarController@show');
Route::get('/cars', 'CreateController@index');
Route::get('/marka', 'MarkaController@index');
Route::post('/marka', 'MarkaController@store');
Route::post('/', 'CreateController@save');
Route::get('/cars/modelis/{car}', 'CarController@showimage');
Route::get('/cars/delete/{car}', 'CarController@delete');

